import { ISalutation } from "./ISalutation";

export class Salutation implements ISalutation {
  greet(nombre: string): string {
      return `¡Hola, ${nombre}!`; // Saludo personalizado
  }
}