import container from "./container";
import { ISalutation } from "./ISalutation";

export function main() {
  // Obtén la instancia del servicio desde el contenedor
  const servicioSaludo = container.get<ISalutation>("ISalutation");

  // Usa el servicio para saludar
  const mensaje = servicioSaludo.greet("Alice");
  console.log(mensaje); // Salida: ¡Hola, Alice!
}

main();
