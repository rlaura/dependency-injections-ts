import { ContainerBuilder } from "node-dependency-injection";
import { Salutation } from "./Salutation";

const container = new ContainerBuilder();

// Registra el servicio Salutation con su interfaz ISalutation
container.register('ISalutation', Salutation);

export default container;