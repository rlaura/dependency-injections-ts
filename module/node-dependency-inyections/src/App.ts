/**
 * Node Dependency Injection
 * https://www.npmjs.com/package/node-dependency-injection
 * https://github.com/zazoomauro/node-dependency-injection/wiki
 */
// export * from "./01-basic-logger/Main";
export * from "./01-basic-logger-yaml/Main";
// export * from "./01-basic-logger-yaml/container/YamlConfig";
// export * from "./01-basic-use/Main";
