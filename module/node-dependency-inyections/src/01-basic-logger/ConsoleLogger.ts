import { ILogger } from "./ILogger";

// Implementación concreta del logger
export class ConsoleLogger implements ILogger {
  log(message: string): void {
    console.log(`[ConsoleLogger] ${message}`);
  }
}
