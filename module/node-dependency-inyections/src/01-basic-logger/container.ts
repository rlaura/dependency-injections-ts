import { ContainerBuilder } from "node-dependency-injection";
import { ConsoleLogger } from "./ConsoleLogger";
import { UserService } from "./UserService";

/**
 * La sintaxis '%Logger%' en addArgument('%Logger%') es una forma
 * de referenciar otro servicio registrado en el contenedor.
 */

// Configuración del contenedor
const container = new ContainerBuilder();
/**
 * Usamos container.register() para registrar nuestras clases.
 * Para UserService, usamos addArgument('%ILogger%') para indicar que debe inyectarse una instancia de ILogger.
 */
// Registramos nuestras dependencias
container.register("ILogger", ConsoleLogger);
container
  .register("UserService", UserService)
  .addArgument(container.get("ILogger"));
// .addArgument('%ILogger%');

// console.log(container.logger);
// // 2. Verificar si un servicio específico está registrado
// console.log("\n¿Está registrado ILogger?", container.has("ILogger"));
// console.log("¿Está registrado UserService?", container.has("UserService"));

// // 3. Obtener definiciones de servicios
// console.log("\nDefinición de UserService:");
// const userServiceDefinition = container.findDefinition("UserService");
// console.log(userServiceDefinition);

// console.log("\nInspección del contenedor:");
// console.log(container.services);

export default container;
