import container from "./container";
import { UserService } from "./UserService";


export function main() {
  // Uso del contenedor
  console.log(container);
  const userService = container.get<UserService>("UserService");
  userService.createUser("JohnDoe");
}

main();
