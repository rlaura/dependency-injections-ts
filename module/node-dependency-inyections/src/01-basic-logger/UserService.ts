import { ILogger } from "./ILogger";

// Un servicio que depende del logger
export class UserService {
  constructor(private logger: ILogger) {}

  createUser(username: string): void {
    console.log('Logger injected:', this.logger);
    this.logger.log(`Creando usuario: ${username}`);
    // Lógica para crear usuario...
  }
}