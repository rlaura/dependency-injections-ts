import { ContainerBuilder, YamlFileLoader } from "node-dependency-injection";
import { join } from "path";
import * as fs from 'fs';

export const containerFun = async () => {
  const container = new ContainerBuilder();
  const yamlPath = join(__dirname, "application_dev.yaml");
  const loader = new YamlFileLoader(container);
  console.log('Loading YAML from:', yamlPath);
  if (!fs.existsSync(yamlPath)) {
    console.error('YAML file not found:', yamlPath);
  } else {
    await loader.load(yamlPath);
    console.log('YAML file loaded successfully');
  }
  return container;
}


// export { container };