import { readdirSync } from "fs";
import "reflect-metadata"; 
//para pnpm creo que se tiene que instalar siempre el ----> pnpm add -D @types/node
import path from "path";
import { ContainerBuilder, YamlFileLoader } from "node-dependency-injection";

const loadContainer = async () => {
  let container = new ContainerBuilder();
  let loader = new YamlFileLoader(container);
  await loader.load(path.resolve(__dirname, 'application_dev.yaml'));
  return container;
};

export default loadContainer;

// const PATH_CONTAINER = `${__dirname}`;
// // console.log(__dirname);

// let container = new ContainerBuilder();
// let loader = new YamlFileLoader(container);
// await loader.load(path.resolve(__dirname, 'application_dev.yaml'));


// export default container;


