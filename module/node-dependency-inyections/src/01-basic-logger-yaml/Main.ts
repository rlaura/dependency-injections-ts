import loadContainer from "./container/YamlConfig";
import { UserService } from "./UserService";


export async function main() {
  const container = await loadContainer();
  console.log(container);

  // Uso del contenedor
  const userService = container.get<UserService>("UserService");
  userService.createUser("JohnDoe");
}

main();
