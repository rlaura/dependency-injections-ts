
// Definimos una interfaz para nuestro logger con la convención IInterface
export interface ILogger {
  log(message: string): void;
}