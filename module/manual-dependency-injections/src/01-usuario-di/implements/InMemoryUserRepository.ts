import { IUserRepository } from "../interfaces/IUserRepository";
import { User } from "./User";

export class InMemoryUserRepository implements IUserRepository {
  private users: User[] = [
    new User(1, "jose", "jose@gmail.com", "123456"),
    new User(2, "carlos", "carlos@gmail.com", "qwert"),
  ];

  async getAllUsers(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      if (this.users) {
        resolve(this.users);
      } else {
        reject(new Error("no hay usuarios"));
      }
    });
  }

  async getUserById(id: number): Promise<User | undefined> {
    return new Promise((resolve, reject) => {
      if (this.users) {
        const usuarioBuscado = this.users.find((usuario) => usuario.id === id);
        resolve(usuarioBuscado);
      } else {
        reject(new Error("no hay usuario"));
      }
    });
  }

  async getUserByIdOrFail(id: number): Promise<User> {
    return new Promise((resolve, reject) => {
      if (this.users) {
        const usuarioBuscado = this.users.find((usuario) => usuario.id === id);
        if (usuarioBuscado) {
          resolve(usuarioBuscado);
        } else {
          reject(new Error("no existe el usuario"));
        }
      } else {
        reject(new Error("no hay usuarios"));
      }
    });
  }

  async addUser(user: User): Promise<void> {
    return new Promise((resolve, reject) => {
      //opcion 1
      this.users.push(user);

      //opcion 2
      // const usuariosActualizados = [...this.users, user];
      // this.users = usuariosActualizados;
      resolve();
    });
  }
}
