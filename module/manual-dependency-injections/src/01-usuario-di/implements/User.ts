export class User {
  private readonly _id: number;
  private readonly _name: string;
  private readonly _email: string;
  private readonly _password: string;

  constructor(id: number, name: string, email: string, password: string) {
    this._id = id;
    this._name = name;
    this._password = password;
    this._email = email;
  }

  public get password(): string {
    return this._password;
  }

  public get name(): string {
    return this._name;
  }

  public get email(): string {
    return this._email;
  }

  public get id(): number {
    return this._id;
  }
}
