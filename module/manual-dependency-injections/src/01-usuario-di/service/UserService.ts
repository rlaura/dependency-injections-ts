import { User } from "../implements/User";
import { IUserRepository } from "../interfaces/IUserRepository";

export class UserService {
  private userRepository: IUserRepository;

  constructor(userRepository: IUserRepository) {
    this.userRepository = userRepository;
  }

  async getAllUsers(): Promise<User[]> {
    return this.userRepository.getAllUsers();
  }

  async getUserById(id: number): Promise<User | undefined> {
    return this.userRepository.getUserById(id);
  }

  async addUser(user: User) {
    this.userRepository.addUser(user);
  }
}
