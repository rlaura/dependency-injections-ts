import { InMemoryUserRepository } from "./implements/InMemoryUserRepository";
import { User } from "./implements/User";
import { UserService } from "./service/UserService";

async function main() {
  // Crear el repositorio en memoria
  // const userRepository = new InMemoryUserRepository();
  // // Crear el servicio de usuarios con el repositorio inyectado
  // const userService = new UserService(userRepository);

  //forma Rapida
  const userService = new UserService(new InMemoryUserRepository());

  // Crear algunos usuarios
  const user1: User = new User(3, "Rene", "rene@gmail.com", "12345");

  // Agregar usuarios a través del servicio
  userService.addUser(user1);

  // Obtener y mostrar todos los usuarios
  const users = await userService.getAllUsers();
  console.log("====== getAllUsers()");
  console.log(users);

  // Obtener y mostrar un usuario por ID
  const user = await userService.getUserById(1);
  console.log("====== getUserById()");
  console.log(user);
}

main();
