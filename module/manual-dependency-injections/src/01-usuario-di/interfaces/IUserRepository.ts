import { User } from "../implements/User";
/**
 * aca como todo se puede la nomenclatura necesaria para el repositorio
 */
export interface IUserRepository {
  //primera forma
  getAllUsers(): Promise<User[]>;
  getUserById(id: number): Promise<User | undefined>;
  //usamos el orFail cuando necesitamos que el dato siempre este presente
  getUserByIdOrFail(id: number): Promise<User>;
  addUser(user: User): Promise<void>;

  //segunda forma
  // allOrFail(): Promise<User[]>;
  // create(user: User) : Promise<void>;
  // ofIdOrFail(id: number): Promise<User>;
}
