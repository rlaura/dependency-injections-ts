import { v4 as uuid } from 'uuid';
import { version as uuidVersion } from 'uuid';
import { validate as uuidValidate } from 'uuid';

export class Uuid {
  constructor(value: string) {
    this.ensureIsValidUuid(value);
  }

  static random(): Uuid {
    return new Uuid(uuid());
  }

  private ensureIsValidUuid(id: string): void {
    if (!this.uuidValidateV4(id)) {
      throw new Error(`<${this.constructor.name}> does not allow the value <${id}>`);
    }
  }

  private uuidValidateV4(uuid:string) {
    return uuidValidate(uuid) && uuidVersion(uuid) === 4;
  }

  value(): string {
    return uuid().toString();
  }
}


// export class Uuid {

//   static value(): string {
//     return uuid();
//   }
// }
