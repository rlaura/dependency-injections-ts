import { DomainEventClass } from "../types/DomainEventClass";
import { DomainEvent } from "./DomainEvent";

export interface IDomainEventSubscriber<T extends DomainEvent> {
  /**
   * Devuelve la lista de clases de eventos de dominio a las que este suscriptor está suscrito.
   * @return {DomainEventClass[]} La lista de clases de eventos de dominio.
   */
  subscribedTo(): DomainEventClass[];

  /**
   * Maneja un evento de dominio.
   * @param {T} domainEvent - El evento de dominio a manejar.
   * @return {Promise<void>} Una promesa que se resuelve una vez que el evento ha sido manejado correctamente.
   */
  on(domainEvent: T): Promise<void>;
}