import { DomainEvent } from "../interfaces/DomainEvent";

export type DomainEventClass = {
  /**
   * El nombre del evento.
   */
  EVENT_NAME: string;
  /**
   * Convierte los parámetros primitivos en una instancia del evento de dominio.
   * @param {string} aggregateId - El ID del agregado asociado al evento.
   * @param {string} eventId - El ID del evento.
   * @param {Date} occurredOn - La fecha y hora en que ocurrió el evento.
   * @param {DomainEventAttributes} attributes - Los atributos del evento.
   * @return {DomainEvent} Una nueva instancia del evento de dominio.
   */
  fromPrimitives(params: {
    aggregateId: string;
    eventId: string;
    occurredOn: Date;
    attributes: DomainEventAttributes;
  }): DomainEvent;
};

/**
 * Define un tipo para los atributos de un evento de dominio.
 */
export type DomainEventAttributes = any;
