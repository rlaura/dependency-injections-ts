import { DomainEvent } from "../interfaces/DomainEvent";
import { IDomainEventSubscriber } from "../interfaces/IDomainEventSubscriber";
import { DomainEventClass } from "../types/DomainEventClass";

// Supongamos que tienes algunas implementaciones de DomainEventSubscriber
export class MyDomainEventSubscriber implements IDomainEventSubscriber<DomainEvent> {

  subscribedTo(): DomainEventClass[] {
    throw new Error("Method not implemented.");
  }

  on(domainEvent: DomainEvent): Promise<void> {
    throw new Error("Method not implemented.");
  }
  // Implementación de métodos
}
