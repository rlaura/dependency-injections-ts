import { AnotherDomainEventSubscriber } from "./implements/AnotherDomainEventSubscriber";
import { DomainEventSubscribers } from "./subscribers/DomainEventSubscribers";
import { MyDomainEventSubscriber } from "./implements/MyDomainEventSubscriber";

// Creas instancias de tus suscriptores de eventos de dominio
const subscriber1 = new MyDomainEventSubscriber();
const subscriber2 = new AnotherDomainEventSubscriber();

// Creas la instancia de DomainEventSubscribers pasando los suscriptores como argumentos
const domainEventSubscribers = new DomainEventSubscribers([subscriber1, subscriber2]);

// Creas la instancia de DomainEventSubscribers pasando las instancias de suscriptores como argumentos
const domainEventSubscribers2 = DomainEventSubscribers.from([subscriber1, subscriber2]);