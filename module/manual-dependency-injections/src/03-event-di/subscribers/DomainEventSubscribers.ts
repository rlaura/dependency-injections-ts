import { DomainEvent } from "../interfaces/DomainEvent";
import { IDomainEventSubscriber } from "../interfaces/IDomainEventSubscriber";

// export class DomainEventSubscribers {
//   constructor(public items: Array<IDomainEventSubscriber<DomainEvent>>) {}
// }


export class DomainEventSubscribers {
  constructor(public items: Array<IDomainEventSubscriber<DomainEvent>>) {}

  static from(subscriberInstances: Array<IDomainEventSubscriber<DomainEvent>>): DomainEventSubscribers {
    return new DomainEventSubscribers(subscriberInstances);
  }
}