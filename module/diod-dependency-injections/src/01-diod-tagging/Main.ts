import "reflect-metadata";
import { ContainerBuilder } from "diod";

abstract class EventBus {
  abstract publish(event: string): void;
  abstract addHandlers(handlers: EventHandler[]): void;
}

// Implementamos la clase MyEventBus que extiende de EventBus
class MyEventBus implements EventBus {
  private handlers: EventHandler[] = [];

  publish(event: string): void {
    console.log(`Evento publicado: ${event}`);
    for (const handler of this.handlers) {
      handler.handle(event);
    }
  }

  addHandlers(handlers: EventHandler[]): void {
    this.handlers.push(...handlers);
  }
}

// Definimos la clase abstracta EventHandler
abstract class EventHandler {
  abstract handle(event: string): void;
}

// Implementamos la clase WhateverEventHandler que extiende de EventHandler
class WhateverEventHandler implements EventHandler {
  handle(event: string): void {
    console.log(`WhateverEventHandler manejando: ${event}`);
  }
}

class FooEventHandler implements EventHandler {
  handle(event: string): void {
    console.log(`FooEventHandler manejando: ${event}`);
  }
}

class BarEventHandler implements EventHandler {
  handle(event: string): void {
    console.log(`BarEventHandler manejando: ${event}`);
  }
}

class Whatever {
  doSomething(): void {
    console.log("Whatever haciendo algo...");
  }
}
class Foo {
  doSomething(): void {
    console.log("Foo haciendo algo...");
  }
}
class Bar {
  doSomething(): void {
    console.log("Bar haciendo algo...");
  }
}

const builder = new ContainerBuilder();

// Registramos MyEventBus como implementación de EventBus y la configuramos como Singleton
builder.register(EventBus).use(MyEventBus).asSingleton();
// Registramos WhateverEventHandler con la etiqueta 'event-handler'
builder.registerAndUse(WhateverEventHandler).addTag("event-handler");
// Registramos FooEventHandler con la etiqueta 'event-handler'
builder.registerAndUse(FooEventHandler).addTag("event-handler");
// Registramos BarEventHandler con la etiqueta 'event-handler'
builder.registerAndUse(BarEventHandler).addTag("event-handler");
// Registramos la clase Whatever como instancia por cada solicitud (asInstancePerRequest)
builder.registerAndUse(Whatever).asInstancePerRequest();
builder.registerAndUse(Foo).asInstancePerRequest();
builder.registerAndUse(Bar).asInstancePerRequest();

// Construimos el contenedor de dependencias
const container = builder.build();

// Ejemplo de uso
const eventBus = container.get<EventBus>(EventBus);

// Resolvemos todos los handlers con la etiqueta 'event-handler'
/**
 * Este fragmento de código tiene la finalidad de obtener todos los manejadores de eventos
 * registrados en el contenedor de dependencias (container) que tengan la etiqueta 'event-handler'.
 *
 * container.findTaggedServiceIdentifiers<EventHandler>('event-handler') -> Devuelve un array de identificadores de servicios que cumplen con esta condición.
 * - container: es el objeto que representa nuestro contenedor de dependencias.
 * - findTaggedServiceIdentifiers<EventHandler>('event-handler'): es un método que busca todos los
 *      identificadores de servicios que están etiquetados con 'event-handler' y que implementan la interfaz EventHandler.
 *
 * .map((identifier) => container.get<EventHandler>(identifier)):
 * - map es un método de los arrays que recorre cada elemento del array y devuelve un nuevo array con
 *   los resultados de llamar a una función proporcionada en cada elemento.
 * - En este caso, identifier representa cada identificador de servicio encontrado en el paso anterior.
 * - container.get<EventHandler>(identifier) es una llamada al método get del contenedor de dependencias,
 *   que recibe como argumento un identificador de servicio y devuelve la instancia del servicio correspondiente.
 *
 * la variable handlers contendrá un array de instancias de todas las clases que implementan EventHandler
 * y que fueron registradas en el contenedor con la etiqueta 'event-handler'. Estas instancias representan
 * los manejadores de eventos disponibles para ser añadidos al eventBus y gestionar los eventos que se publiquen.
 */
const handlers = container
  .findTaggedServiceIdentifiers<EventHandler>("event-handler")
  .map((identifier) => container.get<EventHandler>(identifier));
console.log("===========handlers========");
console.log(handlers);
console.log("===========handlers========");

// Añadimos los handlers al event bus
eventBus.addHandlers(handlers);

// Publicamos un evento para probar
eventBus.publish("Test Event");

const whateverInstance = container.get<Whatever>(Whatever);
whateverInstance.doSomething();

const fooInstance = container.get<Foo>(Foo);
fooInstance.doSomething();

const barteverInstance = container.get<Bar>(Bar);
barteverInstance.doSomething();
