import { ContainerBuilder } from "diod";
import "reflect-metadata";

import { ConsoleLogger, ILogger } from "./Logger";
import { MyDatabase } from "./MyDatabase";
/**
 * Registrar fábricas para crear instancias
 * Puede registrar fábricas para crear instancias complejas.
 * Podrás registrarlos así:
 * 
 * Ahora podrás consultar MyDatabase normalmente y también se inyectará 
 * como cualquier otra dependencia si algún servicio lo solicita en su constructor.
 */


// Creando el contenedor y registrando dependencias.
const builder = new ContainerBuilder();

// Registre ConsoleLogger como implementación de Logger
builder.register(ILogger).use(ConsoleLogger);

// Registrar MyDatabase con una función de fábrica que resuelve Logger desde el contenedor
builder.register(MyDatabase).useFactory((c) => {
  return new MyDatabase("my-connection-string", c.get(ILogger));
});

// construir el contenedor
export const container = builder.build();
