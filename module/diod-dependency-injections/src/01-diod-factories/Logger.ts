// Define the Logger hierarchy
export abstract class ILogger {
  abstract log(message: string): void;
}

export class ConsoleLogger implements ILogger {
  log(message: string) {
    console.log(message);
  }
}