import { container } from "./Container";
import { MyDatabase } from "./MyDatabase";

export function main() {
  // Resolve and use MyDatabase from the container
  const database = container.get(MyDatabase);
  database.query();
}
main();