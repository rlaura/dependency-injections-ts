import { ILogger } from "./Logger";


// Define the MyDatabase class
export class MyDatabase {
  constructor(
    private readonly connectionString: string,
    private readonly logger: ILogger
  ) {}

  query(): void {
    this.logger.log(`Querying database at ${this.connectionString}`);
    // Database query logic would go here
  }
}
