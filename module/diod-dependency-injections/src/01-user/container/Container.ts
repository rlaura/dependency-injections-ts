// Importar reflect-metadata para la inyección de dependencias CON DECORADORES
import "reflect-metadata"; 

import { ContainerBuilder } from "diod";

import { IUserRepository } from "../interface/IUserRepository";
import { InMemoryUserRepository } from "../implements/InMemoryUserRepository";
import { UserService } from "../service/UserService";

const builder = new ContainerBuilder();
builder.register<IUserRepository>(IUserRepository).use(InMemoryUserRepository);
builder.registerAndUse<UserService>(UserService);

const container = builder.build()

export default container;