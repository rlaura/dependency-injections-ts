import { Service } from "diod";

import { User } from "../interface/User";
import { IUserRepository } from "../interface/IUserRepository";

@Service()
export class UserService {
  private userRepository: IUserRepository;

  constructor(userRepository: IUserRepository) {
    this.userRepository = userRepository;
  }

  async getUser(id:string): Promise<User> {
    return await this.userRepository.getUser(id);
  }

  async getall(): Promise<User[]> {
    return await this.userRepository.getall();
  }
}