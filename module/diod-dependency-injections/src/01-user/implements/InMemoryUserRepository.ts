import { Service } from "diod";

import { User } from "../interface/User";
import { IUserRepository } from "../interface/IUserRepository";


@Service()
export class InMemoryUserRepository implements IUserRepository {
  private users = [
    new User("1", "Luis", "luis@gmail.com", "123456"),
    new User("2", "Jose", "jose@gmail.com", "123456"),
    new User("3", "Carlos", "carlos@gmail.com", "123456"),
  ];
  async getUser(id: string): Promise<User> {
    return new Promise((resolve, reject) => {
      const dato = this.users.find((user) => user.id === id);
      if (dato) {
        resolve(dato);
      } else {
        reject(new Error("dato no encontrado"));
      }
    });
  }
  async getall(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      if (this.users.length > 0) {
        resolve(this.users);
      } else {
        reject(new Error("no hay usuarios"));
      }
    });
  }
}
