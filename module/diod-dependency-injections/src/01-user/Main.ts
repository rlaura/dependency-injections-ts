
import container from "./container/Container";
import { UserService } from "./service/UserService";

export async function main() {
  console.log("✅✅✅ Inyeccion de dependencia: FORMA DIOD - 01-user");
  const userService = container.get<UserService>(UserService);

  const user = await userService.getUser("3");
  const users = await userService.getall();

  console.log(user);
  console.log(users);
}
