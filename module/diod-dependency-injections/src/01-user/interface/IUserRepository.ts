import { User } from "./User";
/**
 * Los métodos abstractos se definen usando la palabra clave abstract y no contienen un cuerpo de método. 
 * Sirven como marcadores de posición que deben ser implementados por cualquier subclase que extienda
 */
export abstract class IUserRepository {
  // Método abstracto para obtener un usuario
  abstract getUser(id:string): Promise<User>;
  // Método abstracto para obtener todos los usuarios
  abstract getall(): Promise<User[]>;
}
