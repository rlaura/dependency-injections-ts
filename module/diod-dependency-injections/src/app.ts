/**
 * EMOJIS
 * https://emojidb.org/job-emojis
 * 
 * DOCUMENTACION DE LA BIBLIOTECA DIOD
 * https://github.com/artberri/diod/blob/main/docs/README.md
 * https://github.com/artberri/diod?tab=readme-ov-file#readme
 * 
 */
export * from "./01-diod-factories/Main";
// export * from "./01-diod-abstractions/Main";


// async function executeApp() {
//   try {
//     console.log("🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷 DIOD-dependency-injections ");

//     const startTime = performance.now();
//     // const [module1, module2, module3] = await Promise.all([
//     const [module1] = await Promise.all([
//       // import("./01-user/Main"),
//       import("./02-command-di-v1/Main"),
//       // import("./02-command-di-v2/Main")
//     ]);

//     // await module1.main();
//     await module1;
//     //  await module2.main();
//     //  await module3.main();

//     //  await Promise.all([
//     //   module1.main(),
//     //   module2.main(),
//     //   module3.main()
//     // ]);

//     const endTime = performance.now();
//     const executionTime = endTime - startTime;

//     // console.log("Todos los módulos se ejecutaron");
//     // console.log("🕐 Tiempo de ejecución:", executionTime, "milisegundos");
//   } catch (error) {
//     console.error("❌ ", error);
//   }
// }

// executeApp();
