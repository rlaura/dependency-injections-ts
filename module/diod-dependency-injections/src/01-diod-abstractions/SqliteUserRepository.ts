import { Service } from "diod";
import { UserRepository } from "./UserRepository";
import { User, UserCriteria, UserDto } from "./User";

@Service()
export class SqliteUserRepository implements UserRepository {
  create(userData: UserDto): User {
    // Implementación para crear usuario en SQLite
    console.log(`Creating user ${userData.username}`);
    // Ejemplo de retorno asumiendo estructura de usuario
    return { id: "generated-id", ...userData };
  }

  findBy(userData: UserCriteria): User[] {
    // Implementación para buscar usuarios en SQLite según criterios
    console.log(`Finding users by criteria`);
    // Ejemplo de retorno asumiendo un resultado vacío
    return []; 
  }
}
