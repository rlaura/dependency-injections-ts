import { User } from "./User";

export abstract class Mailer {
  abstract sendConfirmationEmail(userData: User): void;
  abstract sendResetPasswordEmail(userData: User): void;
}