import { Service } from "diod";
import { Mailer } from "./Mailer";
import { User } from "./User";

@Service()
export class AcmeMailer implements Mailer {
  sendConfirmationEmail(userData: User): void {
    // Implementation to send confirmation email
    console.log(`Enviando correo electrónico de confirmación a ${userData.email}`);
  }

  sendResetPasswordEmail(userData: User): void {
    // Implementation to send reset password email
    console.log(`Enviando correo electrónico para restablecer la contraseña a ${userData.email}`);
  }
}