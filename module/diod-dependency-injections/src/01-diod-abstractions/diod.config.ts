import "reflect-metadata"; 

import { ContainerBuilder } from "diod";
import { AcmeMailer } from "./AcmeMailer";
import { Mailer } from "./Mailer";
import { UserRepository } from "./UserRepository";
import { SqliteUserRepository } from "./SqliteUserRepository";
import { SignUpUseCase } from "./SignUpUseCase";

/**
 * Usar abstracciones para definir sus dependencias
 * 
 * La D de los principios SOLID se refiere a la inversión de dependencia.
 * Este principio anima a los desarrolladores a utilizar abstracciones para 
 * definir dependencias en determinadas situaciones. Las abstracciones generalmente 
 * se definen con interfaces en otros lenguajes, pero las interfaces Typecript no 
 * están disponibles en tiempo de ejecución y es por eso que DIOD requiere clases abstractas 
 * para las abstracciones si desea que se conecten automáticamente.
 * 
 * 
 * 
 */
const builder = new ContainerBuilder();

// Registrar implementaciones concretas
builder.register(Mailer).use(AcmeMailer);
builder.register(UserRepository).use(SqliteUserRepository);

// Registre SignUpUseCase con la anotación del servicio DIOD
builder.registerAndUse(SignUpUseCase);


// Construye el contenedor
const container = builder.build();

export default container;