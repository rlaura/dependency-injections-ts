import container from "./diod.config";
import { SignUpUseCase } from "./SignUpUseCase";
/**
 * https://github.com/artberri/diod/blob/main/docs/abstractions.md
 * 
 * 
 * La D de los principios SOLID se refiere a la inversión de dependencia. 
 * Este principio anima a los desarrolladores a utilizar abstracciones para 
 * definir dependencias en determinadas situaciones. Las abstracciones generalmente 
 * se definen con interfaces en otros lenguajes, pero las interfaces Typecript no están 
 * disponibles en tiempo de ejecución y es por eso que DIOD requiere clases abstractas para 
 * las abstracciones si desea que se conecten automáticamente.

 * 
 */
const signUpUseCase = container.get(SignUpUseCase);
signUpUseCase.execute({
  username: 'exampleUser',
  email: 'user@example.com',
  // Other user data as needed
});