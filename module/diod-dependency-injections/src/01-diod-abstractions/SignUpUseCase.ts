// application/use-cases/SignUpUseCase.ts
import { Service } from 'diod'
import { UserRepository } from './UserRepository'
import { Mailer } from './Mailer'
import { UserDto } from './User'

@Service()
export class SignUpUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly mailer: Mailer
  ) {}

  execute(userData: UserDto): void {
    const user = this.userRepository.create(userData)
    this.mailer.sendConfirmationEmail(user)
  }
}