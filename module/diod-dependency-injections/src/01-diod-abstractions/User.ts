export interface User {
  id: string;
  username: string;
  email: string;
  // Otros campos según sea necesario
}

// domain/User.ts
export interface UserDto {
  username: string;
  email: string;
  // Otros campos según sea necesario para la creación de usuarios
}

// domain/User.ts
export interface UserCriteria {
  // Campos para la búsqueda de usuarios
  username?: string;
  email?: string;
  // Otros campos según sea necesario para los criterios de búsqueda
}