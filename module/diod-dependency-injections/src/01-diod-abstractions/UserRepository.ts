
import { User, UserCriteria, UserDto } from "./User";

export abstract class UserRepository {
  abstract create(userData: UserDto): User;
  abstract findBy(userData: UserCriteria): User[];
}