import { ICommand } from "../interface/ICommand";

export class CreateUserCommand implements ICommand {
  constructor(public username: string, public password: string) {}
}