import { AwilixContainer } from "awilix";
import { ICommand } from "./ICommand";
import { ICommandHandler } from "./ICommandHandler";

// export class CommandBus {
//   private container: AwilixContainer;

//   constructor(container: AwilixContainer) {
//     this.container = container;
//   }

//   async execute<T extends ICommand>(command: T): Promise<void> {
//     const handlerName = `${command.constructor.name}Handler`;
//     console.log(`Resolviendo manejador: ${handlerName}`);
//     const handler = this.container.resolve<ICommandHandler<T>>(handlerName);
//     await handler.handle(command);
//   }
// }

export class CommandBus {
  private container: AwilixContainer;

  constructor({ container }: { container: AwilixContainer }) {
    this.container = container;
  }

  async execute<T extends ICommand>(command: T): Promise<void> {
    const handlerName = `${command.constructor.name}Handler`;
    console.log(`Resolviendo manejador: ${handlerName}`);
    const handler = this.container.resolve<ICommandHandler<T>>(handlerName);
    await handler.handle(command);
  }
}