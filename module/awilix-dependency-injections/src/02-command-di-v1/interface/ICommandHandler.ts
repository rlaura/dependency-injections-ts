import { ICommand } from "./ICommand";

// ICommandHandler.ts
export interface ICommandHandler<T extends ICommand> {
  handle(command: T): Promise<void>;
}