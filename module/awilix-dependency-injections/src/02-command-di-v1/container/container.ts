import { asClass, createContainer } from "awilix";
import { CommandBus } from "../interface/CommandBus";
import { CreateUserCommandHandler } from "../handlers/CreateUserCommandHandler";

const container = createContainer();

// container.register({
//   commandBus: asClass(CommandBus).singleton(),
//   CreateUserCommandHandler: asClass(CreateUserCommandHandler).singleton(), // Nombre exacto
// });

container.register({
  //El contenedor se inyecta en CommandBus utilizando inject(() => ({ container })).
  commandBus: asClass(CommandBus).inject(() => ({ container })).singleton(),
  // Asegurarse de que el nombre del manejador coincida con el nombre esperado
  CreateUserCommandHandler: asClass(CreateUserCommandHandler).singleton(),
});


// console.log(container.registrations);  // Log the registered dependencies

export default container;