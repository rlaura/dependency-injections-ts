import { CreateUserCommand } from "./commands/CreateUserCommand";
import container from "./container/container";
import { CommandBus } from "./interface/CommandBus";

// Uso del CommandBus
export async function main() {
  console.log("✅✅✅ Inyeccion de dependencia: FORMA AWILIX - 02-command-di-v1");
  const commandBus = container.resolve<CommandBus>("commandBus");
  const command = new CreateUserCommand("jose", "password123");
  await commandBus.execute(command);
}
// main();