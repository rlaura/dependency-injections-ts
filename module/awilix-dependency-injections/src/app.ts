// Leer documentacion
// https://github.com/jeffijoe/awilix#readme
console.log("======================= Inyeccion de dependencia: FORMA AWILIX - APP =======================");

// export * from "./01-user/Main";
// export * from "./02-command-di-v1/Main";
// export * from "./02-command-di-v2/Main";

// const imports = [
//   import("./01-user/Main"),
//   import("./02-command-di-v1/Main"),
//   import("./02-command-di-v2/Main")
// ];

/**
 * //TODO:ejecutando los modulos de forma paralela con Promise.all y .then()
 */
// Promise.all(imports)
//   .then(([module1, module2, module3]) => {
//     // Ejecutar la función main de cada módulo de forma asíncrona
//     return Promise.all([
//       module1.main(),
//       module2.main(),
//       module3.main()
//     ]);
//   })
//   .then(() => {
//     // Todos los main() se han ejecutado exitosamente
//     console.log("Todos los main() se ejecutaron exitosamente.");
//   })
//   .catch(error => {
//     // Manejo de errores
//     console.error("Error al ejecutar main() de los módulos:", error);
//   });

/**
 * //TODO:ejecutando los modulos de forma secuencial con .then()
 */
// import("./01-user/Main")
//   .then(module1 => {
//     // Ejecutar la función main() del módulo 1
//     return module1.main();
//   })
//   .then(() => {
//     // Después de que se haya ejecutado el módulo 1,
//     // importar y ejecutar el módulo 2
//     return import("./02-command-di-v1/Main");
//   })
//   .then(module2 => {
//     // Ejecutar la función main() del módulo 2
//     return module2.main();
//   })
//   .then(() => {
//     // Después de que se haya ejecutado el módulo 2,
//     // importar y ejecutar el módulo 3
//     return import("./02-command-di-v2/Main");
//   })
//   .then(module3 => {
//     // Ejecutar la función main() del módulo 3
//     return module3.main();
//   })
//   .then(() => {
//     // Todos los módulos se han ejecutado en orden
//     console.log("Todos los módulos se ejecutaron en orden.");
//   })
//   .catch(error => {
//     // Manejo de errores
//     console.error("Error al ejecutar los módulos:", error);
//   });

/**
 * //TODO:ejecutando los modulos de forma secuencial con async y await
 */
// async function runModulesSequentially() {
//   try {
//     const module1 = await import("./01-user/Main");
//     await module1.main();

//     const module2 = await import("./02-command-di-v1/Main");
//     await module2.main();

//     const module3 = await import("./02-command-di-v2/Main");
//     await module3.main();

//     console.log("Todos los módulos se ejecutaron en orden.");
//   } catch (error) {
//     console.error("Error al ejecutar los módulos:", error);
//   }
// }

/**
 * //TODO:ejecutando los modulos de forma paralela y en secuencia, con medicion de tiempo de ejecucion
 *        con async y await
 */
async function runModulesSequentially() {
  try {
    console.log("🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷🔷 AWILIX-dependency-injections ");
    const startTime = performance.now();
    const [module1, module2, module3] = await Promise.all([
      import("./01-user/Main"),
      import("./02-command-di-v1/Main"),
      import("./02-command-di-v2/Main")
    ]);

     await module1.main();
     await module2.main();
     await module3.main();

    //  await Promise.all([
    //   module1.main(),
    //   module2.main(),
    //   module3.main()
    // ]);

    const endTime = performance.now();
    const executionTime = endTime - startTime;

    console.log("Todos los módulos se ejecutaron");
    console.log("🕐 Tiempo de ejecución:", executionTime, "milisegundos");
  } catch (error) {
    console.error("Error al ejecutar los módulos:", error);
  }
}

runModulesSequentially();