// Importar reflect-metadata para la inyección de dependencias
import "reflect-metadata"; 

import { createContainer, asClass, InjectionMode } from "awilix";
import { UserRepository } from "../implements/UserRepository";
import { UserService } from "../service/UserService-classic";

/**
 * InyecciónMode.CLASSIC: 
 * analiza los parámetros de función/constructor y los relaciona con los registros en el contenedor. 
 * El modo CLASSIC tiene un costo de inicialización ligeramente mayor ya que tiene que analizar la función/clase 
 * para descubrir las dependencias en el momento del registro; sin embargo, resolverlas será mucho más rápido 
 * que cuando se usa PROXY. ¡No uses CLASSIC si minimizas tu código! Recomendamos utilizar CLASSIC en Node y PROXY 
 * en entornos donde se necesita minificación.
 * 
 * Además, si la clase tiene una clase base pero no declara un constructor propio, 
 * Awilix simplemente invoca el constructor base con las dependencias que requiera.
 */
const container = createContainer({
  injectionMode: InjectionMode.CLASSIC,
});

container.register({
  // Registro de UserRepository
  userRepository: asClass(UserRepository).scoped(),
  // Registro de UserService
  userService: asClass(UserService).scoped(),
});

export default container;

// container.register({
//   // Registrando UserRepository como un singleton
//   userRepository: asClass(UserRepository).singleton(),
//   // Registrando UserService con su dependencia UserRepository
//   userService: asClass(UserService)
//     .scoped()
//     .inject(() => ({
//       userRepository: container.cradle.userRepository,
//     })),
// });
// Registrar clases en el contenedor
