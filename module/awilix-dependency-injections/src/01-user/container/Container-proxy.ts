// Importar reflect-metadata para la inyección de dependencias
import "reflect-metadata"; 

import { createContainer, asClass, InjectionMode } from "awilix";
import { UserRepository } from "../implements/UserRepository";
import { UserService } from "../service/UserService-proxy";

/**
 * InyecciónMode.PROXY (predeterminado):
 * inyecta un proxy a funciones/constructores que parece un objeto normal.
 */

// Crea el contenedor y establece el modo de inyección en PROXY (que también es el valor predeterminado).
// Habilite el modo estricto para realizar comprobaciones de corrección adicionales (muy recomendable).
const container = createContainer({
  injectionMode: InjectionMode.PROXY,
  strict: true,
});

container.register({
  // Registro de UserRepository
  userRepository: asClass(UserRepository).scoped(),
  // Registro de UserService
  userService: asClass(UserService).scoped(),
});

export default container;

// container.register({
//   // Registrando UserRepository como un singleton
//   userRepository: asClass(UserRepository).singleton(),
//   // Registrando UserService con su dependencia UserRepository
//   userService: asClass(UserService)
//     .scoped()
//     .inject(() => ({
//       userRepository: container.cradle.userRepository,
//     })),
// });
// Registrar clases en el contenedor
