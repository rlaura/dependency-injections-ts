import { UserRepository } from "../implements/UserRepository";

export class UserService {
  private userRepository: UserRepository;

  constructor({ userRepository }: { userRepository: UserRepository }) {
    this.userRepository = userRepository;
  }

  async getUsers(): Promise<string[]> {
    return await this.userRepository.getUsers();
  }
}
