export class UserRepository {
  async getUsers(): Promise<string[]> {
    // Simulando una operación asincrónica, por ejemplo, una consulta a una base de datos
    return new Promise<string[]>((resolve) => {
      setTimeout(() => {
        resolve(["User 1", "User 2", "User 3"]);
      }, 2000);
    });
  }
}
