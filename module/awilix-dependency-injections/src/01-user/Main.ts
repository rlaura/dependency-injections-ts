//review:PARA LA FORMA PROXY EN AWILIX
import container from "./container/Container-proxy";
import { UserService } from "./service/UserService-proxy";

//review:PARA LA FORMA CLASSIC EN AWILIX
// import container from "./container/Container-classic";
// import { UserService } from "./service/UserService-classic";

export async function main() {
  console.log("✅✅✅ Inyeccion de dependencia: FORMA AWILIX - 01-user");
  /**
   * Las dos formas funcionan pero la primera se ve mas profesional
   */
  const userService = container.resolve<UserService>("userService");
  // const userService: UserService = container.resolve("userService");

  const users = await userService.getUsers();

  console.log(users);
}

// main();
