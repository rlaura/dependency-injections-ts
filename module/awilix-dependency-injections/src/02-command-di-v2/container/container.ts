import { asClass, asFunction, createContainer } from "awilix";
import { Database } from "../commands/Database";
import { CreateUserHandler } from "../handlers/createUserHandler";
import { CommandBus } from "../interface/commandBus";

const container = createContainer();

container.register({
  database: asClass(Database).singleton(),
  createUserHandler: asClass(CreateUserHandler).singleton(),
  commandBus: asFunction(() => {
    const bus = new CommandBus();
    bus.register('CreateUserCommand', container.resolve<CreateUserHandler>('createUserHandler'));
    return bus;
  }).singleton()
});

export default container;