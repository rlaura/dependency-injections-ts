
import { CreateUserCommand } from "../commands/CreateUserCommand";
import { Database } from "../commands/Database";

export class CreateUserHandler {
  private database: Database;

  constructor({database}:{database: Database}) {
    this.database = database;
  }

  async initialize() {
    await this.database.connect();
  }

  async handle(command: CreateUserCommand) {
    console.log(`creando usuario: ${command.username}`);
    // Lógica para crear un usuario
  }
}