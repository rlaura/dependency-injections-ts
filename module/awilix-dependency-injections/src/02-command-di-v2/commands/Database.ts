export class Database {
  async connect() {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        console.log("CONECTANDO A LA BASE DE DATOS: 2 seg");
        resolve();
      }, 2000);
    });
  }
}