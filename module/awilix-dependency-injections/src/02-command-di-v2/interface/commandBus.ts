export class CommandBus {
  private handlers: Map<string, any>;

  constructor() {
    this.handlers = new Map();
  }

  register(commandName: string, handler: any) {
    this.handlers.set(commandName, handler);
  }

  async dispatch(commandName: string, command: any) {
    const handler = this.handlers.get(commandName);
    if (!handler) {
      throw new Error(`No handler registered for ${commandName}`);
    }
    await handler.handle(command);
  }
}