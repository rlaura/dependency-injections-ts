import { CreateUserCommand } from "./commands/CreateUserCommand";
import container from "./container/container";
import { CreateUserHandler } from "./handlers/createUserHandler";
import { CommandBus } from "./interface/commandBus";


export async function main() {
  console.log("✅✅✅ Inyeccion de dependencia: FORMA AWILIX - 02-command-di-v2");
  const commandBus = container.resolve<CommandBus>('commandBus');
  const createUserHandler = container.resolve<CreateUserHandler>('createUserHandler');
  
  // Espera a que el handler se inicialice
  await createUserHandler.initialize();
  
  const command: CreateUserCommand = {
    username: 'Luis Comunica',
    password: 'password123'
  };
  
  await commandBus.dispatch('CreateUserCommand', command);
}

// main();
