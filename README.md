# Proyecto node typescript monorepositorio multipaquete con workspaces
## ¿Que son los workspaces?
Los `workspaces` (espacios de trabajo) son una característica proporcionada por herramientas como Yarn y npm 
que permiten gestionar múltiples paquetes dentro de un mismo repositorio como un solo proyecto. 
Esta característica es especialmente útil para crear monorepositorios multipaquete y para el desarrollo de microservicios.
### Gestión de dependencias compartidas
Los workspaces permiten compartir dependencias entre múltiples paquetes dentro de un monorepo. 
Esto significa que puedes especificar las dependencias una sola vez en el nivel superior del monorepo 
y todos los paquetes internos utilizarán las mismas versiones de las dependencias compartidas. 
Esto simplifica la gestión de dependencias y reduce la duplicación de paquetes.
### Versionamiento coherente
Al utilizar workspaces, puedes garantizar que todas las dependencias dentro de tu monorepo estén utilizando 
versiones coherentes entre sí. Esto ayuda a evitar problemas de compatibilidad y garantiza que tus paquetes funcionen bien juntos.
### Compartir código y recursos
Los workspaces facilitan el compartir código y otros recursos entre los diferentes paquetes dentro del monorepo. Puedes crear módulos compartidos, bibliotecas de utilidades y componentes reutilizables que sean accesibles desde todos los paquetes en el monorepo.
### Gestión centralizada
Al tener todos tus paquetes en un solo repositorio, la gestión del código fuente, las pruebas, 
las compilaciones y el despliegue se simplifican significativamente. Puedes utilizar herramientas de automatización 
para ejecutar tareas en todos los paquetes de manera coordinada.
### Facilita la implementación de microservicios
Los monorepos multipaquete pueden utilizarse como la base para sistemas basados en microservicios. 
Cada paquete en el monorepo puede representar un servicio individual, y los workspaces facilitan la gestión de la dependencia 
y el despliegue de estos servicios de manera coherente.
## ¿Que es pnpm?
`pnpm` es un administrador de paquetes para Node.js. Al igual que npm (Node Package Manager) y Yarn, pnpm se utiliza para instalar, 
administrar y distribuir paquetes de Node.js en tus proyectos. Sin embargo, pnpm ofrece algunas características únicas que 
lo diferencian de npm y Yarn.
### Instalación eficiente
`pnpm` utiliza un mecanismo de almacenamiento en caché y enlaces simbólicos para compartir dependencias entre los proyectos.
### Espacio en disco reducido
`pnpm` utiliza enlaces simbólicos para compartir dependencias entre los proyectos, lo que significa que no hay duplicación de paquetes en el disco. 
Esto resulta en un uso de espacio en disco reducido en comparación con npm y Yarn, 
especialmente en proyectos con múltiples dependencias compartidas.
## Creando un proyecto node
### Iniciación de un Proyecto Node.js
Para iniciar un proyecto Node.js, sigue estos pasos:

1. Abre tu terminal.
2. En la `carpeta raiz` y en las `subcarpetas` que seran los `subproyectos paketes o modulos` ejecutar el comando.
```bash
pnpm init
```
### Instalación e Iniciación de un Proyecto TypeScript

1. Instala TypeScript como una dependencia de desarrollo en la carpeta raiz ejecutando el siguiente comando:
```bash
pnpm i typescript -D
```

2. Crea un archivo `tsconfig.json` en la raíz de tu proyecto para configurar TypeScript. Puedes hacerlo ejecutando:
```bash
npx tsc --init
```
### Instalación de ts-node y nodemon

1. Instala `ts-node` para poder ejecutar y depurar archivos TS directamente en Node ejecutando el siguiente comando:
```bash
pnpm i -D ts-node
```

2. Instala `nodemon` ejecutando el siguiente comando:
```bash
pnpm i nodemon -D
```
### comandos en pnpm
1. para `pnpm run -C <path> <command>`
- **Uso del directorio:** -C <path> cambia al directorio especificado antes de ejecutar el comando.
- **Directorio de trabajo:** El comando se ejecuta como si estuvieras en el directorio del subpaquete.
- **Específico para rutas:** Este método es útil si deseas especificar la ruta exacta al subpaquete
```bash
pnpm run -C module/awilix-dependency-injections dev
```
Este comando cambiará al directorio `module/awilix-dependency-injections` y ejecutará el script `dev` definido en el `package.json` de ese subpaquete.

2. para `pnpm --filter <package> run <command>`
- **Uso de filtros:** `--filter <package>` filtra el paquete por nombre.
- **Directorio de trabajo:** El comando se ejecuta en el contexto del paquete filtrado.
- **Flexibilidad:** Permite usar nombres de paquetes en lugar de rutas. Puedes usar patrones para filtrar múltiples paquetes.
```bash
pnpm --filter awilix-dependency-injections run dev
```
Este comando buscará el paquete `awilix-dependency-injections` en tu monorepositorio y ejecutará el script `dev` definido en su package.json.

3. para `pnpm --recursive`
- El `--recursive` o `-r` indica a `pnpm` que debe buscar en todos los paquetes del monorepositorio y ejecutar el script especificado (build en este caso) en cada uno de ellos.

```bash
pnpm --recursive run dev
```
- Buscará todos los paquetes dentro del monorepositorio.
- En cada paquete, verificará si hay un script build definido en su package.json.
- Si el script build está definido, lo ejecutará.

### configuracion del monorepositorio
1. Primeramente en `"workspaces":` colocar en el arreglo los subproyectos
```json
{
  "name": "dependency-injections-ts",
  "private": true,
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "workspaces": [
    "module/*"
  ],
  "scripts": {
    "build:module": "pnpm --recursive run build",
    "start:module": "pnpm --recursive run start",
    "start2:module": "pnpm --recursive run start",
    "dev:module": "pnpm --recursive run dev",
    "dev:awilix": "pnpm --filter awilix-dependency-injections run dev",
    "dev:diod": "pnpm run -C module/diod-dependency-injections dev",
    "dev:manual": "pnpm run -C module/manual-dependency-injections dev",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "nodemon": "^3.1.1",
    "ts-node": "^10.9.2",
    "typescript": "^5.4.5"
  }
}
```

2. Crear un archivo `pnpm-workspace.yaml` y colocar en `packages:` el nombre de la carpeta que engloba todos los subproyectos
```yaml
packages:
  - 'module/*'
```
